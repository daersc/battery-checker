# Battery Checker
This repository contains a simple script which checks if the battery of the
system is low. Its result is sent to libnotify.

## How it works
Execute the `check-battery` script, e.g.
```
$ ./check-battery
```
Optionally you may pass tresholds for the critical battery state. Per default,
the battery is in critical state on 20% of charge or below.

If battery is in critical state and has state *Discharging* a notification is
sent using `notify-send` command.

### Add a cronjob
Add a cronjob to periodically check if the battery is low. Such a line in a
users crontab could look like
```
*/5 * * * * DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$UID/bus /usr/local/bin/check-battery
```
to check for battery /BAT0/ every 5 minutes. The part
`DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$UID/bus` is necessary
to allow invoking notify-send from background scripts.
